<?php
namespace app;
echo "Now I'm in Line#".__LINE__."<br><br>";


echo "Now I'm inside the file: ".__FILE__."<br><br>";

function doSomething(){

    echo "Now I'm inside the: ".__FUNCTION__."<br><br>";
}
doSomething();

class myClass{
    public function myMethod(){
        echo "Now I'm inside the: ".__METHOD__."<br><br>";

    }
}
$objMyClass = new myClass();

$objMyClass->myMethod();


echo "<br> I'm inside : " .__DIR__;

trait myTrait {

    public function  myTraitMethod() {
        echo "<br> I'm inside : " .__TRAIT__;
    }

}
class myClass1{

    use myTrait;

}

$objMyClass1 = new myClass1();

$objMyClass1->myTraitMethod();

echo "<br> I'm inside the :".__NAMESPACE__." Namespace";