<?php

//floatval()

$myVar = "454.554 Hello World";
$result = floatval($myVar);
echo $result;

echo "<br>";

//empty()

$myVar = "is not empty";

$result = empty($myVar);

var_dump($result);

echo "<br>";
// is_array()

$x = array(null,true,44,44.44);
$y = 5443;

var_dump(is_array($x));

class myClass{
    public function myMethod(){
        echo "Now I'm inside the: ".__METHOD__;

    }
}
$obj1 = new myClass();
$obj2 = new myClass();
$obj3 = new myClass();
$obj4 = new myClass();


$myArr = array($obj1,$obj2,$obj3,$obj4);

echo "<pre>";
var_dump($myArr);
echo "</pre>";